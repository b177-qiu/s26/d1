// requiring the HTTP module

let http = require("http");

// Creating the server
// createServer() method

http.createServer(function (request, response) {
	response.writeHead(200,{'Content-Type':'text/plain'});
	response.end('Hello, World from Batch 177');
}).listen(4000);

// 200 - status code
// 4000 port
// writeHead method - contains status code for the response
// end method - end the response process 


// Create a console log to indicate that the server is running

console.log('Server running at localhost: 4000');

